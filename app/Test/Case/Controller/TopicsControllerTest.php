<?php
App::uses('TopicsController', 'Controller');

/**
 * TopicsController Test Case
 */
class TopicsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.topic',
		'app.user',
		'app.comment',
		'app.post'
	);

}
